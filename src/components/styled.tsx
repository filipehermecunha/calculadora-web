import styled from 'styled-components' 
 
export const Tecla = styled.button`
    display: flex;
    padding: 1rem; 
    justify-content: center;
    align-items: center;
    gap: 0.625rem;
    border: none;
    border-radius: 0.5rem;
    background: #181C2A;
    color: #F8F9FC;
    font-family: Inter;
    font-size: 1.5rem;
    font-weight: 400;
`
export const Molde = styled.div`
    display: grid;
    grid-template-columns: auto auto auto auto; 
    grid-column-gap: 1rem;
    grid-row-gap: 1rem;
`

export const TwoColumn = styled.button`
    display: flex;
    padding: 1rem; 
    justify-content: center;
    align-items: center;
    gap: 0.625rem; 
    border: none;
    border-radius: 0.5rem;
    background: #181C2A;
    grid-column: span 2;
    color: #F8F9FC;
    font-family: Inter;
    font-size: 1.5rem;
    font-weight: 400;
`

export const TwoRow = styled.button`
    display: flex;
    padding: 1rem; 
    justify-content: center;
    align-items: center;
    gap: 0.625rem;
    border: none;
    border-radius: 0.5rem;
    background: #181C2A;
    grid-row: span 2;
    color: #F8F9FC;
    font-family: Inter;
    font-size: 1.5rem;
    font-weight: 400;
`

export const Visor = styled.div`
    height: 3rem;
    margin-bottom: 1.5rem;
    flex-shrink: 0;
    border-radius: 0.625rem;
    background: #252D4A;

    color: white;
    font-size: x-large;
    text-align: right;
    padding-right: 1rem;
    
    display: flex;
    align-items: center;
    justify-content: right;
`