import './App.css'
import './App.css'
import { Tecla,Molde,TwoColumn,TwoRow,Visor } from './components/styled'
import { InputProvider,useInput } from './components/input'

function App() {
  let valor:string
  let operador:string
  let resul:number
  
   const OPvisor = () => {

    const {firstNumber,secondNumber,operator} = useInput();

    return(
      <Visor>
        <p> {firstNumber}{operator}{secondNumber}</p>
      </Visor>
    );
   } 

   const Teclado: React.FC = () => {
    const {addFirstNumber,firstNumber,addSecondNumber,secondNumber,changeOperator,operator} = useInput();
    
    const InputNum = (e: React.FormEvent| any) => {
      e.preventDefault();
      valor = e.target.value.toString();

      if(firstNumber != "0" && operator != ""){
        
        if (secondNumber == "" && valor === '.' ) {
          valor = '0.'
        }
        if (valor === '.' && secondNumber.includes(".")) {
          return
         }

        if(secondNumber.slice(-1) == "0" && secondNumber.includes(".")){      
          addSecondNumber(secondNumber + valor)
        } 
        
        addSecondNumber(secondNumber + valor )
      } 
      else
      {  
        if (firstNumber == "" && valor === '.' ) {
          valor = '0.'
        }
        if (valor === '.' && firstNumber.includes(".")) {
          return
        }
        
        if(firstNumber.slice(-1) == "0" && firstNumber.includes(".")){      
          addFirstNumber(firstNumber + valor)
        } 
        
        addFirstNumber(firstNumber + valor)
      }
    };
  
    const inputOperador = (e: React.FormEvent| any) => {
      e.preventDefault();
      operador = e.target.value.toString();

      if(firstNumber == "" && secondNumber == "")
      {
        changeOperator('')
      }
      else if(firstNumber != "" && secondNumber == ""){
        changeOperator(operador)
      }
      else{
        changeOperator(operador)
        addFirstNumber(firstNumber + operator + secondNumber) 
        addSecondNumber('')
      }  
    }
  
    const Clear = () => {
      addFirstNumber('')
      addSecondNumber('')
      changeOperator('')
    }
    
    const ClearUM = () => {

      if(operator != "" && secondNumber == "")
      {
        changeOperator(operator.toString().slice(0, -1));
      }
      else if(firstNumber != "" && secondNumber == "")
      {
        addFirstNumber(firstNumber.toString().slice(0, -1));
      } 
      else
      {
        addSecondNumber(secondNumber.toString().slice(0, -1));
      } 
    }

    function calculate() {
    
      addSecondNumber('')
      changeOperator('')

      if(operator == '/' && secondNumber == '0'){
        alert('Operação Inválida: Divisão por 0')
        addFirstNumber('')
      }
      else {
        resul = eval(firstNumber + operator + secondNumber);
        addFirstNumber(resul.toString())
      }
    }

    return(
      <Molde>       
            <Tecla onClick={ClearUM} >C</Tecla>
            <Tecla onClick={Clear}>CC</Tecla>
            <Tecla onClick={inputOperador} value={"*"}>*</Tecla>
            <Tecla onClick={inputOperador} value={"/"}>/</Tecla>

            <Tecla onClick={InputNum} value={7}>7</Tecla>
            <Tecla onClick={InputNum} value={8}>8</Tecla>
            <Tecla onClick={InputNum} value={9}>9</Tecla>
            <Tecla onClick={inputOperador} value={"-"}>-</Tecla>

            <Tecla onClick={InputNum} value={4}>4</Tecla>
            <Tecla onClick={InputNum} value={5}>5</Tecla>
            <Tecla onClick={InputNum} value={6}>6</Tecla>
            <Tecla onClick={inputOperador} value={"+"}>+</Tecla>

            <Tecla onClick={InputNum} value={1}>1</Tecla>
            <Tecla onClick={InputNum} value={2}>2</Tecla>
            <Tecla onClick={InputNum} value={3}>3</Tecla>
            <TwoRow onClick={calculate} value={"="}>=</TwoRow> 

            <TwoColumn onClick={InputNum} value={0}>0</TwoColumn>
            <Tecla onClick={InputNum} value={"."}>.</Tecla>
      </Molde>
    );
   } 

  return (
      <InputProvider>
         
            <OPvisor />
                               
            <Teclado />
          
        </InputProvider>
  )
}

export default App