# DESAFIO CALCULADORA WEB

# Questões Teóricas

## 1. O que é um contexto?

Em React, "contexto" (context) é uma funcionalidade que permite compartilhar dados entre componentes sem a necessidade de passá-los manualmente em cada nível. Isso é útil quando muitos componentes aninhados precisam acessar os mesmos dados. O contexto é criado usando React.createContext(), fornecido aos componentes usando Context.Provider, e consumido usando useContext ou Context.Consumer. No entanto, é recomendado usar contexto com moderação, pois pode tornar o código mais complexo se usado em excesso.

## 2. O que são React Hooks?

Os React Hooks são funções que permitem que você use o estado e outros recursos do React em componentes funcionais. Você pode usar os Hooks built-in ou combiná-los para construir o seu próprio. 

**Exemplos de Hooks built-in:** useState, useContext, useEffect,entre outros.

## 3. O que esse contexto possibilita? 

**Armazenamento Centralizado do Estado:** O contexto mantém um estado centralizado para as variáveis firstNumber, operator, e secondNumber. Isso permite que outros componentes acessem e modifiquem esses valores sem a necessidade de passar esses dados por meio de props através da árvore de componentes.

**Métodos para Modificar o Estado:** O contexto fornece métodos como addFirstNumber, changeOperator, e addSecondNumber que podem ser usados para atualizar os valores das variáveis de estado. Esses métodos são passados para os consumidores do contexto por meio da propriedade value do InputContext.Provider.

**Reutilização do Estado e Lógica:** A lógica relacionada ao gerenciamento de input está encapsulada no contexto. Isso permite a reutilização fácil do estado e da lógica associada em diferentes partes do aplicativo, evitando duplicação de código.

**Consumo Simples por Componentes:** Componentes que precisam interagir com o estado relacionado à entrada podem usar o useInput hook para consumir o contexto. Essa abstração simplifica o acesso ao estado global, tornando o código mais limpo e mais fácil de entender.

**Mensagem de Erro Clara:** A função useInput verifica se está sendo usada dentro do contexto adequado. Se não estiver, ela lança um erro, garantindo que os desenvolvedores sejam informados se estiverem tentando usar o hook fora de um provedor InputProvider.
